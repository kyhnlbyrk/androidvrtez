﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Ghosts : MonoBehaviour {
	public GameObject sp; //spawnPoint
	public GameObject wp; //WayPoint
	private NavMeshAgent nma;
	private List<Transform> wpList = new List<Transform>();
	private Transform goal;
	private Transform currentDestination;
	private int frameCountForFrightened;
	private int frameCount;
	private bool isChase;
	private bool isScatter;
	private bool isScared = false;
	private bool isFrameCount; //Hold the data for if frameCountForFrightened will be calculated or not
	public GameObject killScreen;

	void Start () {
		Screen.orientation = ScreenOrientation.Landscape;
		killScreen.SetActive (false);
		frameCount = 1;
		isScatter = true;
		isChase = false;
		frameCountForFrightened = 0;
		transform.position = sp.transform.position;
		foreach (Transform wp in wp.transform) {
			wpList.Add(wp);
		}
		ChooseWayPoint (wpList);
		nma = GetComponent<NavMeshAgent> ();
	}
	
	// Update is called once per frame
	void Update () {
		isScared = Pacman.isBigEat;
		if (isFrameCount) {
			frameCountForFrightened++;
		}
		if (frameCountForFrightened == 330) {
			isScared = false;
			frameCountForFrightened = 0;
			isFrameCount = false;
			isScatter = true;
			isChase = false;
			frameCount = 0;
		}
		frameCount++;
		int x = Pacman.count;//When other ghosts start to walk.
		if (x == 0 || x < 2) {
			GameObject.FindGameObjectWithTag("Pinky").transform.position = new Vector3(-5f, 0f, 0f);
			GameObject.FindGameObjectWithTag ("Blinky").transform.position = new Vector3 (1f, 0f, 4.82f);
			GameObject.FindGameObjectWithTag ("Clyde").transform.position = new Vector3 (-5.04f, 0f, 4.82f);
		}else if (x >= 2 && x<8) {
			GameObject.Find ("Blinky").transform.position = new Vector3 (1f, 0f, 4.82f);
			GameObject.Find ("Clyde").transform.position = new Vector3 (-5.04f, 0f, 4.82f);
		} else if (x >= 8 && x< 15) {
			GameObject.Find ("Clyde").transform.position = new Vector3 (-5.04f, 0f, 4.82f);
		}
		if (isScatter && frameCount <= 300 && !isScared) {
			nma.speed = 3.1f;
			Debug.Log ("Scatter");
			ChooseWayPoint (wpList);
			nma.SetDestination (goal.transform.position);
			nma.destination = goal.position;
			isScatter = false;
			isChase = true;
		} else if (isChase && frameCount <= 900 && frameCount > 300 && !isScared) {
			nma.speed = 3.1f;
			Debug.Log ("Chase");
			goal = GameObject.FindGameObjectWithTag ("Player").transform;
			nma.destination = goal.position;
			if (frameCount >= 900) {
				frameCount = 0;
				isChase = false;
				isScatter = true;
			}
		}else if (isScared) {
			nma.speed = 2.5f;
			Debug.Log ("Scared");
			isFrameCount = true;
			isScatter = false;
			isChase = false;
			ChooseWayPoint (wpList);
			nma.SetDestination (goal.transform.position);
			nma.destination = goal.position;

		}

	}

	public void ChooseWayPoint(List<Transform> list){
		System.Random rnd = new System.Random ();
		int numb = rnd.Next (0, list.Count);
		goal = list [numb];

	}

	void OnTriggerEnter(Collider other){
		if (!isScared) {
			if (Pacman.score > Pacman.highScore) {
				PlayerPrefs.SetInt ("Score", Pacman.score);
			}
			killScreen.SetActive (true);
			restart ();

		} else {
			nma.enabled = false;
			transform.position = sp.transform.position;//Reset the position like respawning
			nma.enabled = true;
			isScared = false;
			isScatter = true;
		}
	}

	void restart(){
		SceneManager.LoadScene (0);
	}
}
