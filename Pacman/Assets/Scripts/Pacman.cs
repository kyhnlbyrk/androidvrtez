﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pacman : MonoBehaviour {
	
	private float gravity;
	private CharacterController cc;
	public CardboardHead head;
	public float speed;
	public static int score; //for holding the score
	public static int count; //for holding how many dots pacman eat
	public static bool isBigEat = false;
	public Text scoreText;
	private static int frameCount;
	private bool isFrameCount; //Hold the data for if frameCount will be calculated or not
	public GameObject teleport1;
	public GameObject teleport2;
	public GameObject sword;
	private AudioSource eatSound;
	private AudioSource bigEatSound;
	private AudioSource hitSound;
	public GameObject finish;
	public Text finishText;
	public Text highScoreText;
	public static int highScore;
	public Animator hand;

	// Use this for initialization
	void Start () {
		count = 0;
		eatSound = GameObject.FindGameObjectWithTag("eatSound").GetComponent<AudioSource>();
		bigEatSound = GameObject.FindGameObjectWithTag("bigEatSound").GetComponent<AudioSource>();
		hitSound = GameObject.FindGameObjectWithTag("hitSound").GetComponent<AudioSource>();
		sword.SetActive (false);
		frameCount = 0;
		highScore = PlayerPrefs.GetInt("Score");
		highScoreText.text = "High Score : " + highScore;
		isFrameCount = false;
		Application.targetFrameRate = 30; // FPS is 30 !
		QualitySettings.vSyncCount = 0;
		cc = GetComponent<CharacterController> ();
		score = 0;
		scoreText.text = "Score: " + score;
	}


	void FixedUpdate(){
		if (isFrameCount) {
			frameCount++;
		}
		if (frameCount == 330) {
			isBigEat = false;
			sword.SetActive (false);
			isFrameCount = false;
			frameCount = 0;
			Debug.Log (frameCount);
		}
		gravity -= 9.81f * Time.fixedDeltaTime;
		cc.Move (new Vector3 (head.Gaze.direction.x * Time.fixedDeltaTime * speed, gravity, head.Gaze.direction.z * Time.fixedDeltaTime * speed));
		if (cc.isGrounded) {
			gravity = 0;
		}
	}

	void OnTriggerEnter(Collider other){
		if(other.gameObject.CompareTag("PickUp")){
			eatSound.Play ();
			other.gameObject.SetActive(false);
			score = score + 10;
			count = count + 1;
			scoreText.text = "Score: " + score;
			if (count == 90) {
				finish.SetActive (true);
				finishText.text = "You Win";
				if (score > highScore) {
					PlayerPrefs.SetInt ("Score", score);
				}
				SceneManager.LoadScene (0);
			}
		}else if (other.gameObject.CompareTag ("BigPickUp")) {
			bigEatSound.Play ();
			other.gameObject.SetActive (false);
			isBigEat = true;
			isFrameCount = true;
			sword.SetActive (true);
		}
		if ((other.gameObject.CompareTag ("Inky") || other.gameObject.CompareTag ("Blinky") ||
		   other.gameObject.CompareTag ("Clyde") || other.gameObject.CompareTag ("Pinky")) &&
		   isBigEat == true) {
			hitSound.Play ();
			hand.SetTrigger ("Hit");
			score = score + 200;
			scoreText.text = "Score: " + score;
		}

		if (other.gameObject.CompareTag ("Teleport1")) {
			transform.position = teleport2.transform.position;
		}

		if (other.gameObject.CompareTag ("Teleport2")) {
			transform.position = teleport1.transform.position;
		}


	}
	
	// Update is called once per frame

}
